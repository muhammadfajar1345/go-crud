 FROM golang:1.19.4-bullseye
 
 COPY go.mod go.sum /appi/
 
 WORKDIR /appi

 RUN go mod download

 COPY . .

 RUN go get -u github.com/gin-gonic/gin

 RUN go get -u gorm.io/gorm

 RUN go get -u gorm.io/driver/postgres

 RUN go build -o /entrypoint

 RUN chmod +x /entrypoint

#  CMD [ "go run main.go" ]

 ENTRYPOINT [ "/entrypoint" ]