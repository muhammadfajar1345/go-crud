package initializers

import (
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

func ConnecToDB() {
	var err error
	dsn := "host=172.19.0.2 user=postgres password=P@ssw0rd dbname=open-data port=5432 sslmode=disable"
	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal("Failed To Connect Database")
	}

}
