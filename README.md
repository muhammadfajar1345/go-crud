# GO CRUD

This is CRUD sample using Golang , Gin and postgress

## Installation

run migration.go first for migrate database scheme

```bash
go run migrate.go
```


## Training Source

Thankyou codingwithrobby to help me learn w/ golang

see this video if you want to learn deeply
https://www.youtube.com/@codingwithrobby
